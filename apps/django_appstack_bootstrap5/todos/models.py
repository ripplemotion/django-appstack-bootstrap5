from datetime import date
from typing import Optional

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser
from django.db import models

from .constants import todo_priority

User: AbstractBaseUser = get_user_model()


class List(models.Model):
    title: str = models.CharField(max_length=255, blank=False)
    user: User = models.ForeignKey(User, on_delete=models.CASCADE, related_name="created_lists")

    def __str__(self) -> str:
        return self.title


class Todo(models.Model):
    list: List = models.ForeignKey(List, on_delete=models.CASCADE, related_name="todos")
    content: str = models.CharField(max_length=255, blank=False)
    due_date: Optional[date] = models.DateField(blank=True, null=True, default=None)
    priority: Optional[str] = models.CharField(
        max_length=max(len(x) for x in todo_priority.all),
        choices=[(x, x) for x in sorted(todo_priority.all)],
        blank=True,
        null=True,
        default="",
    )

    def __str__(self) -> str:
        return self.content
