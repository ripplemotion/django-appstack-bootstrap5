__all__ = ["todo_priority"]


class TodoPriority:
    low = "low"
    medium = "medium"
    high = "high"
    all = {low, medium, high}


todo_priority = TodoPriority()
