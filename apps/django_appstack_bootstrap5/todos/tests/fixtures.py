import uuid

# from datetime import date, timedelta
from typing import Optional

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractBaseUser

from ..models import List, Todo

User: AbstractBaseUser = get_user_model()


class TodoMixin:
    def any_user(self) -> User:
        return User.objects.create(username=uuid.uuid1().hex)

    def any_list(self, user: Optional[User] = None) -> List:
        user = user or self.any_user()
        lst = List.objects.create(title=f"list {uuid.uuid1().hex}", user=user)

        Todo.objects.create(list=lst, content="le ménage")
        Todo.objects.create(list=lst, content="la vaisselle")
        Todo.objects.create(list=lst, content="le sport")

        return lst

    # def any_todo(
    #     self,
    #     todo_list: Optional[List] = None,
    #     content: Optional[str] = None,
    #     due_date: Optional[date] = None,
    #     priority: Optional[str] = None,
    # ) -> Todo:
    #     content = content or "Content Example"
    #     due_date = due_date or date.today() + timedelta(days=1)
    #     todo_list = todo_list or self.any_list()

    #     todo = Todo.objects.create(list=todo_list, content=content, due_date=due_date, priority=priority)

    #     return todo
