from django.test import TestCase
from hamcrest import assert_that, is_

from .fixtures import TodoMixin


class ToDoListTests(TestCase, TodoMixin):
    def test_list_str(self) -> None:
        """
        given any list
        its string representation is its title
        """
        lst = self.any_list()

        assert_that(str(lst), is_(lst.title))

    def test_todo_str(self) -> None:
        """
        given any list
        its todo representation is its content
        """
        lst = self.any_list()
        for todo in lst.todos.all():
            assert_that(str(todo), is_(todo.content))

    # def test_any_todo(self):
    #     for state in todo_states.all:
    #         with self.subTest(state=state):
    #             td = self.any_todo(state=state)
    #             assert_that(td.state, is_(state))
