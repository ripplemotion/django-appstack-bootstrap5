import os
from typing import Any, Dict

import dj_database_url
from django.utils.log import DEFAULT_LOGGING

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# SECURITY WARNING: keep the secret key used in production secret!
# Generate one with: python -c 'from django.utils.crypto import get_random_string; print (get_random_string(50, "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)"))' # noqa
SECRET_KEY = "1%qq%2j5yq0u=^*^7d38b5pqk7dmvj5p0dz1d@d4(_6lcm4^7p"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ["localhost", ".ripplemotion.fr"]

INSTALLED_APPS = [
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django_extensions",
    "raven.contrib.django.raven_compat",
    "django_appstack_bootstrap5.todos",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

ROOT_URLCONF = "django_appstack_bootstrap5.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

WSGI_APPLICATION = "django_appstack_bootstrap5.wsgi.application"

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

DATABASES = {
    "default": dj_database_url.config(
        env="POSTGRESQL_URL",
        default="postgresql://ripple@localhost:5432/django_appstack_bootstrap5",
    )
}

LANGUAGE_CODE = "en-us"

TIME_ZONE = "Europe/Paris"

USE_I18N = True

USE_L10N = True

USE_TZ = True

MEDIA_ROOT = os.path.expanduser("~/.django-appstack-bootstrap5/media/")

MEDIA_URL = "/media/"

STATIC_ROOT = os.path.expanduser("~/.django-appstack-bootstrap5/static/")

STATIC_URL = "/static/"

RAVEN_CONFIG: Dict[str, Any] = {
    "auto_log_stacks": True,
}

REDIS_URL = os.getenv("REDIS_URL", "redis://localhost:6379")

CACHES = {
    "default": {
        "BACKEND": "redis_cache.RedisCache",
        "LOCATION": REDIS_URL,
        "CACHE_MIDDLEWARE_KEY_PREFIX ": "django_appstack_bootstrap5",
    }
}

DATADOG_TRACE = {
    "DEFAULT_SERVICE": "django-appstack-bootstrap5",
}

SESSION_COOKIE_NAME = "django-appstack-bootstrap5"

LOGGING = DEFAULT_LOGGING
LOGGING["loggers"]["django-appstack-bootstrap5"] = {"handlers": ["console", "sentry"], "level": "INFO"}
LOGGING["formatters"]["simple"] = {"format": "%(asctime)s %(levelname)s %(message)s"}

LOGGING["handlers"]["console"].update({"level": "DEBUG", "formatter": "simple"})
LOGGING["handlers"]["sentry"] = {
    "level": "ERROR",  # To capture more than ERROR, change to WARNING, INFO, etc.
    "class": "raven.contrib.django.raven_compat.handlers.SentryHandler",
}
