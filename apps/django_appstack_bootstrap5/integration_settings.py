# flake8: noqa
from django_appstack_bootstrap5.settings import *

DEBUG = False
TEMPLATE_DEBUG = False

STATIC_ROOT = "/usr/local/ripple/django-appstack-bootstrap5-dev.ripplemotion.fr/shared/public/static/"
MEDIA_ROOT = "/usr/local/ripple/django-appstack-bootstrap5-dev.ripplemotion.fr/shared/public/media/"

MEDIA_URL = "https://django-appstack-bootstrap5-dev.ripplemotion.fr/media/"

SESSION_ENGINE = "django.contrib.sessions.backends.cache"

# FIXME Change according to project
RAVEN_CONFIG["dsn"] = ""

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTOCOL", "https")

INSTALLED_APPS += [
    "ddtrace.contrib.django",
]
