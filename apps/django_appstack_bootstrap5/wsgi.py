from __future__ import unicode_literals

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_appstack_bootstrap5.settings")

application = get_wsgi_application()
