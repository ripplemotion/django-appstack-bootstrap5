# flake8: noqa
import os

from django_appstack_bootstrap5.settings import *

DEBUG = True

INSTALLED_APPS += ()


MEDIA_URL = "{}/media/".format(os.environ.get("PUBLIC_ENDPOINT", "http://localhost:8000"))

ALLOWED_HOSTS += ["django-appstack-bootstrap5-api"]
