from setuptools import find_packages, setup

setup(
    name="django-appstack-bootstrap5-api",
    version="1.0",
    long_description="",
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[],
)
