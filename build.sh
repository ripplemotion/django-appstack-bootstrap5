#!/bin/bash

set -e

WERCKER='./build/bin/wercker'

pushd `dirname $0`
[ -e ~/.ssh/id_rsa ] && echo export RIPPLEMOTION_KEY_PRIVATE=\"`cat ~/.ssh/id_rsa | awk 1 ORS='\\\\n'`\" > local.env
[ -e ~/.ssh/id_dsa ] && echo export RIPPLEMOTION_KEY_PRIVATE=\"`cat ~/.ssh/id_dsa | awk 1 ORS='\\\\n'`\" > local.env
mkdir -p `dirname $WERCKER`

if [ ! -e $WERCKER ] && [ $(uname) = "Darwin" ]
then
    curl https://s3.amazonaws.com/downloads.wercker.com/cli/versions/1.0.1555/darwin_amd64/wercker -o $WERCKER && chmod +x $WERCKER
fi

if [ ! -e $WERCKER ] && [ $(uname) = "Linux" ]
then
    curl https://s3.amazonaws.com/downloads.wercker.com/cli/versions/1.0.1555/linux_amd64/wercker -o $WERCKER && chmod +x $WERCKER
fi

$WERCKER build
popd
