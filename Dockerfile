FROM ripplemotion/geodjango-box:pyenv
ENV PYTHONUNBUFFERED 1
ENV LC_ALL en_US.UTF-8
RUN mkdir -p /code/
WORKDIR /code
ADD .python-version /code/
ADD requirements.txt /code/
RUN eval "$(pyenv init -)" && pip install --upgrade pip setuptools
RUN grep "^-e git+ssh" -v requirements.txt > public_requirements.txt
RUN eval "$(pyenv init -)" && pip install --cache-dir=/code/cache -r public_requirements.txt && rm -rf /code/cache/
ADD build/packages /code/packages
RUN eval "$(pyenv init -)" && if test "$(ls -A packages)"; then pip install packages/*; fi
ADD apps /code/apps
RUN eval "$(pyenv init -)" && pip install -e apps
ADD wait-for-it.sh manage.py url_to_netloc /code/
ADD Procfile.docker /code/Procfile
ENV CELERY_WORKERS 1
ENV DJANGO_SETTINGS_MODULE django_appstack_bootstrap5.container_settings
ENV C_FORCE_ROOT 1
ENTRYPOINT eval "$(pyenv init -)" && \
    ./wait-for-it.sh `./url_to_netloc $KAFKA_URL` -t 180 -- echo "kafka is up" && \
    ./wait-for-it.sh $POSTGRESQL_HOST:$POSTGRESQL_PORT -t 180 -- echo "postgres is up" && \
    sleep 5 && \
    (createdb -U $POSTGRESQL_USER -h $POSTGRESQL_HOST -p $POSTGRESQL_PORT $POSTGRESQL_DB || true) && \
    python manage.py migrate && \
    python manage.py createcachetable && \
    python manage.py ping_kafka && \
    # python manage.py replicate_all && \
    exec honcho start
HEALTHCHECK CMD curl http://localhost:8000/admin/
