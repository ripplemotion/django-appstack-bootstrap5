upstream django-appstack-bootstrap5.ripplemotion.fr_app_server {
    server unix:/tmp/django-appstack-bootstrap5.ripplemotion.fr.sock fail_timeout=0;
}

server {
    listen 443 ssl;
    server_name django-appstack-bootstrap5.ripplemotion.fr;

    ssl_certificate /etc/letsencrypt/live/django-appstack-bootstrap5.ripplemotion.fr/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/django-appstack-bootstrap5.ripplemotion.fr/privkey.pem;

    keepalive_timeout 5;

    root /usr/local/ripple/django-appstack-bootstrap5.ripplemotion.fr/shared/public/;

    location / {
        try_files $uri @proxy_to_app;
    }

    location @proxy_to_app {
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header Host $http_host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-Protocol https;
        proxy_redirect off;
        proxy_pass http://django-appstack-bootstrap5.ripplemotion.fr_app_server;
    }

    access_log /var/log/django-appstack-bootstrap5.ripplemotion.fr/access.log timed_combined_ripple;
    error_log /var/log/django-appstack-bootstrap5.ripplemotion.fr/error.log error;
}

server {
    listen 80;
    server_name django-appstack-bootstrap5.ripplemotion.fr;

    root /usr/local/ripple/django-appstack-bootstrap5.ripplemotion.fr/shared/public/;

    location ^~ /.well-known/ {
    	allow all;
      default_type "text/plain";
    }

    location / {
       rewrite ^ https://$host$request_uri? permanent;
    }
}
