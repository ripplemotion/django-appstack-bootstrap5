#!/bin/bash

set -e

cd "$(dirname "$0")"

IMAGE=quay.io/ripplemotion/django-appstack-bootstrap5
BRANCH_NAME=${BRANCH_NAME:-`git rev-parse --abbrev-ref HEAD`}
BRANCH_NAME=${BRANCH_NAME/\//-}
tox -e package
docker build -t $IMAGE:$BRANCH_NAME .

if [ $BRANCH_NAME == "master" ]; then
   TAG_NAME=`git name-rev --name-only --tags --exclude 'live*' HEAD`
   if [ ! -z "$TAG_NAME" ] && [ "$TAG_NAME" != "undefined" ]; then
      docker tag $IMAGE:$BRANCH_NAME $IMAGE:$TAG_NAME
      docker push $IMAGE:$TAG_NAME
   fi;
fi;


if [ "$1" != "--no-push" ]
then
  docker push $IMAGE:$BRANCH_NAME
fi
