# django-appstack-bootstrap5


# deploy

```
tox -e ansible integration deploy   # deploy to integration
tox -e ansible integration dump_db  # dump integration db
tox -e ansible all deploy           # deploy to all
```

## Run test

```
docker-compose up -d
tox
```

## run ci

```
./build.sh
```

## build container

```
./build-container.sh
```
